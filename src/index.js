import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.winningPos !== -1? <em>{props.value}</em> : props.value }
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        key = {"sqr " + i}
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
        winningPos={this.props.winningPositions? this.props.winningPositions.indexOf(i) : -1}
      />
    );
  }

  renderRowOfSquares(n){
    let squares = [];
    for(let i = n; i < n+3; i++){
        squares.push(this.renderSquare(i))
    }
    return squares;
  }

  renderRow(n){
    return(
      <div>
        {this.renderRowOfSquares(n)}
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.renderRow(0)}
        {this.renderRow(3)}
        {this.renderRow(6)}
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      stepNumber: 0,
      xIsNext: true,
      move_indices: [],
      isAsc: true
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    const move_indices = this.state.move_indices.slice(0, this.state.stepNumber)

    if (calculateWinner(squares) || squares[i]) {
      return;
    }

    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({
      history: history.concat([
        {
          squares: squares
        }
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
      move_indices: move_indices.concat(i)
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    });
  }

  getMoves(history) {
      const moves = history.map((step, move) => {
      const sqr_index = move ? this.state.move_indices[move-1]: move;
      var i = 0;
      var j = 0;
      if (sqr_index > 0) {
        j = sqr_index % 3;
        i = (sqr_index - j) / 3;
      }
      const desc = move ?
        'Go to move #' + move + ' at position (' + i +',' + j + ')':
        'Go to game start';

      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>
            { move === this.state.stepNumber? <b>{desc}</b> : desc}
          </button>
        </li>
      );
    });
    return moves
  }

  changeMovesOrder(){
    this.setState({
      isAsc: !this.state.isAsc
    });

  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winningPositions = calculateWinner(current.squares);
    const winner = (winningPositions ? current.squares[winningPositions[0]] : null);

    const moves = this.getMoves(history);

    let status;
    if (winner) {
      status = "Winner: " + winner;
    } else {
      status = "Yo the next player is: " + (this.state.xIsNext ? "X" : "O");
    }
    if (this.state.stepNumber === 9 && !winningPositions ){
      // Its a draw!
      status = "Its a draw!"
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={i => this.handleClick(i)}
            winningPositions={winningPositions}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <button onClick = {() => this.changeMovesOrder()}> Sort moves by: {this.state.isAsc ? "Asc" : "Desc"} </button>
          <ol>{ this.state.isAsc ? moves : moves.reverse()}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(<Game />, document.getElementById("root"));

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return lines[i];
    }
  }
  return null;
}
